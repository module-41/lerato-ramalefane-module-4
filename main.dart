import 'package:flutter/material.dart';

void main() {
  runApp(
    const MaterialApp(
      home: PageOne(),
    ),
  );
}

class PageOne extends StatelessWidget {
  const PageOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(_createRoute());
          },
          child: const Text('Go!'),
        ),
      ),
    );
  }
}

Route _createRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => const PageTwo(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1.0);
      const end = Offset.zero;
      const curve = Curves.ease;

      final tween = Tween(begin: begin, end: end);
      final curvedAnimation = CurvedAnimation(
        parent: animation,
        curve: curve,
      );

      return SlideTransition(
        position: tween.animate(curvedAnimation),
        child: child,
      );
    },
  );
}

class PageTwo extends StatefulWidget {
  const PageTwo({Key? key}) : super(key: key);

  @override
  State<PageTwo> createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  final inboxSnackBar = SnackBar(
    content: Text(
      'You selected inbox',
    ),
  );

  final sentSnackBar = SnackBar(
    content: Text(
      'You selected sent',
    ),
  );

  final draftSnackBar = SnackBar(
    content: Text(
      'You selected draft',
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: const Center(
        child: Text('Page Two'),
      ),
      drawer: Drawer(
          child: Column(
        children: [
          ListView(
            children: [
              ListTile(
                leading: Icon(
                  Icons.inbox,
                  size: 35,
                  color: Colors.greenAccent,
                ),
                title: Text(
                  'Inbox',
                  style: TextStyle(color: Colors.greenAccent),
                ),
                onTap: () {
                  ScaffoldMessenger.of(context).showSnackBar(inboxSnackBar);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.sentiment_satisfied,
                  size: 35,
                  color: Colors.greenAccent,
                ),
                title: Text(
                  'Sent',
                  style: TextStyle(color: Colors.greenAccent),
                ),
                onTap: () {
                  ScaffoldMessenger.of(context).showSnackBar(sentSnackBar);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.drafts,
                  size: 35,
                  color: Colors.greenAccent,
                ),
                title: Text(
                  'Draft',
                  style: TextStyle(color: Colors.greenAccent),
                ),
                onTap: () {
                  ScaffoldMessenger.of(context).showSnackBar(draftSnackBar);
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ],
      )),
    );
  }
}
